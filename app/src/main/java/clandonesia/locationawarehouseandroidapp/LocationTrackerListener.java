package clandonesia.locationawarehouseandroidapp;

import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;

/**
 * Created by alfa on 2/26/17.
 */

public interface LocationTrackerListener extends ConnectionCallbacks, OnConnectionFailedListener, ResultCallback {
    @Override
    public void onConnected(Bundle bundle);

    @Override
    public void onConnectionSuspended(int i);

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult);

    @Override
    public void onResult(Result result);
}
