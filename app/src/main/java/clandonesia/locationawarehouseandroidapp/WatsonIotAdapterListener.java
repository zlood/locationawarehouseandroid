package clandonesia.locationawarehouseandroidapp;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

/**
 * Created by Lenovo on 22/02/2017.
 */

public interface WatsonIotAdapterListener extends IMqttActionListener {
    @Override
    void onSuccess(IMqttToken asyncActionToken);

    @Override
    void onFailure(IMqttToken asyncActionToken, Throwable exception);
}
