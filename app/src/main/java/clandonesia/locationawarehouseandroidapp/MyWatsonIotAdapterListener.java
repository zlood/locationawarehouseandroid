package clandonesia.locationawarehouseandroidapp;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttToken;

/**
 * Created by Lenovo on 22/02/2017.
 */

public class MyWatsonIotAdapterListener implements WatsonIotAdapterListener {

    private final static String TAG = MyWatsonIotAdapterListener.class.toString();

    private final Context context;
    private final Constants.IotAction action;
    private Location centreOfLocation;
    private LocationTracker locationTracker;

    public MyWatsonIotAdapterListener(Context context, Constants.IotAction action){
        this.context = context;
        this.action = action;
    }

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        switch (action){
            case CONNECTING:
                processIotConnect(asyncActionToken);
                break;
            case DISCONNECTING:
                processIotDisconnect(asyncActionToken);
                break;
            case PUBLISH:
                processIotPublish(asyncActionToken);
                break;
            case SUBSCRIBE:
                processIotSubscribe(asyncActionToken);
        }

    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        switch (action){
            case CONNECTING:
                failIotConnect(exception);
                break;
            case DISCONNECTING:
                failIotDisconnect(exception);
                break;
            case PUBLISH:
                failIotPublish(exception);
                break;
            case SUBSCRIBE:
                failIotSubscribe(exception);
                break;
        }
    }

    private void processIotConnect(IMqttToken token){
        Log.d(TAG, "Watson is connected successfully!");

        centreOfLocation = new Location("");
        centreOfLocation.setLatitude(-6.435664);
        centreOfLocation.setLongitude(106.752093);
        locationTracker = LocationTracker.initLocationTracker(context, centreOfLocation, 500);

        WatsonIotAdapterImpl iotAdapter = WatsonIotAdapterImpl.getInstance(context);
        MyWatsonIotAdapterListener listener = new MyWatsonIotAdapterListener(context, Constants.IotAction.SUBSCRIBE);
        //iotAdapter.watsonIotSubscribeEvent("peripheral_state", "json", 0, "subscribe", listener);
        iotAdapter.watsonIotSubscribeEvent("peripheral_state", "json", Constants.DEVICE_TYPE_CONTROLLER, "+", 0, "subscribe", listener);

    }

    private void processIotDisconnect(IMqttToken token){
        Log.d(TAG, "Watson is now disconnected");
    }

    private void processIotPublish(IMqttToken token){
        Log.d(TAG, "Successfully published..");
    }

    private void processIotSubscribe(IMqttToken token){
        Log.d(TAG, "Successfully subscribed..");
    }

    private void failIotConnect(Throwable exception){
        Log.d(TAG, "Client fails to connect, exception: " + exception.getMessage(), exception.getCause());
    }

    private void failIotDisconnect(Throwable exception){
        Log.d(TAG, "Fail to disconnect Watson, exception:", exception.getCause());
    }

    private void failIotPublish(Throwable exception){
        Log.d(TAG, "Client fails to publish, exception:" + exception.getMessage());
    }

    private void failIotSubscribe(Throwable exception){
        Log.d(TAG, "Client fails to subscribe, exception:" + exception.getMessage(), exception.getCause());
    }
}
