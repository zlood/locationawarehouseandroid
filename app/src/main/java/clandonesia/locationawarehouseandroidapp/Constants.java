package clandonesia.locationawarehouseandroidapp;

/**
 * Created by alfa on 2/25/17.
 */

public class Constants {
    public final static String APP_ID = "clandonesia.locationawarehouseandroidapp";

    public enum IotAction {CONNECTING, DISCONNECTING, SUBSCRIBE, UNSUBSCRIBE, PUBLISH, UNPUBLISH}

    /* Geofence Parameters*/
    public static long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 1000000;
    public static String GEOFENCE_REQUEST_ID = "HouseGeofence";

    /* Geofence Event Error Codes */
    public static int GEOFENCE_NOT_AVAILABLE = 1000;
    public static int GEOFENCE_TOO_MANY_GEOFENCES = 1001;
    public static int GEOFENCE_TOO_MANY_PENDING_INTENTS = 1002;

    /* Mqtt Topics and Events */
    public static String IOTDEVICE_STATUS_EVENT = "peripheral_state";

    /* Intents */
    public final static String INTENT_DATA = "data";
    public final static String INTENT_DATA_RECEIVED = "received";
    public final static String INTENT_IOT_DEVICE_STATUS = "device_status";
    public final static String INTENT_DEVICE_ID = "device_id";

    /* Device Type */
    public final static String DEVICE_TYPE_ANDROID = "Android";
    public final static String DEVICE_TYPE_CONTROLLER = "Home_Controller";

}

