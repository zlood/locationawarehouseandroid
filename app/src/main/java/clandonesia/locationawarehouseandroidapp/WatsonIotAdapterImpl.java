package clandonesia.locationawarehouseandroidapp;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.*;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.internal.security.SimpleBase64Encoder;
import org.eclipse.paho.client.mqttv3.util.Strings;

/**
 * Created by alfa on 2/12/17.
 */

public class WatsonIotAdapterImpl{

    private static final String TAG = WatsonIotAdapterImpl.class.getName();
    private static final String IOT_ORGANIZATION_TCP = ".messaging.internetofthings.ibmcloud.com:1883";
    private static final String IOT_DEVICE_USERNAME  = "use-token-auth";
    private static final String ORGANIZATION_TEMP =  "cqf8b6";
    private static final String DEVICETYPE_TEMP = "Android";
    private static final String DEVICEID_TEMP = "123456";
    private static final String AUTHTOKEN_TEMP = "Clandonesia123";
    private static final String APIKEY_TEMP = "a-cqf8b6-mbvw3dhon8";
    private static final String APP_AUTHTOKEN_TEMP = "ASB?pV_lWaHMNWQFHz";
    private static final boolean isMqttApplication = true;

    private final Context context;
    private WatsonIotAdapterModel iotmodel;
    private MqttAndroidClient client;
    private static WatsonIotAdapterImpl instance;

    private String organization;
    private String deviceType;
    private String deviceId;
    private String authToken;

    private WatsonIotAdapterImpl(Context context){
        this.context = context;
        iotmodel = new WatsonIotAdapterModel();

        iotmodel.setOrganization(ORGANIZATION_TEMP);
        iotmodel.setDeviceType(DEVICETYPE_TEMP);
        iotmodel.setDeviceId(DEVICEID_TEMP);
        iotmodel.setUsername(APIKEY_TEMP);
        iotmodel.setAuthToken(APP_AUTHTOKEN_TEMP);
    }

    private WatsonIotAdapterImpl(Context context, String organization, String deviceId, String deviceType, String authToken){
        this.context = context;
        iotmodel = new WatsonIotAdapterModel();

        //to be implemented
    }

    public static WatsonIotAdapterImpl getInstance(Context context){
        Log.d(TAG, ".getInstance() entered");
        if (instance == null){
            Log.d(TAG, "new WatsonIotAdapter instance..");
            instance = new WatsonIotAdapterImpl(context);
        }
        return instance;
    }

    private static WatsonIotAdapterImpl getInstance(Context context, String organization, String deviceId, String deviceType, String authToken){
        //to be implemented
        return null;
    }

    public void watsonIotConnect(){

        String brokerURI = "tcp://" + iotmodel.getOrganization() + IOT_ORGANIZATION_TCP;
        /* Device client ID */
        //final String clientID = "d:" + iotmodel.getOrganization() + ":" + iotmodel.getDeviceType() + ":" + iotmodel.getDeviceId();
        final String clientID = "a:" + iotmodel.getOrganization() + ":" + "androidapp";

        String username = iotmodel.getUsername();
        char[] password = iotmodel.getAuthToken().toCharArray();

        if(!isClientConnected()){
            client = new MqttAndroidClient(context, brokerURI, clientID);

            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);
            options.setUserName(username);
            options.setPassword(password);

            try{
                MyWatsonIotAdapterListener listener = new MyWatsonIotAdapterListener(context, Constants.IotAction.CONNECTING);
                client.connect(options, context, listener);
                client.setCallback(MyIoTClientCallback.getInstance(context));
            }
            catch (MqttException e){
                e.printStackTrace();
            }
        }
    }

    public void watsonIotDisconnect(){

        if(isClientConnected()){
            try {
                MyWatsonIotAdapterListener listener = new MyWatsonIotAdapterListener(context, Constants.IotAction.DISCONNECTING);
                client.disconnect(context, listener);

            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

    }

    public void watsonIotSubscribeCommand(String command, String format, String deviceType, String deviceId, int qos, Object userContext, IMqttActionListener listener) {

        String commandTopic = getCommandTopic(command, deviceType, deviceId, format);

        if(isClientConnected()){
            try{
                client.subscribe(commandTopic, qos, userContext, listener);
            }
            catch (MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotUnsubscribeCommand(String command, String format, String deviceType, String deviceId, Object userContext, IMqttActionListener listener) {

        String commandTopic = getCommandTopic(command, deviceType, deviceId, format);

        if(isClientConnected()){
            try{
                client.unsubscribe(commandTopic, userContext, listener);
            }
            catch (MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotSubscribeEvent(String event, String format, String deviceType, String deviceId, int qos, Object userContext, IMqttActionListener listener) {

        String eventTopic = getEventTopic(event, deviceType, deviceId, format);
        Log.d(TAG, "Event Topic: " +eventTopic);

        if (isClientConnected()){
            try{
                client.subscribe(eventTopic, qos,  userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotSubscribeEvent(String event, String format, int qos, Object userContext, IMqttActionListener listener) {

        String eventTopic = getEventTopic(event, format);
        Log.d(TAG, "Event Topic: " +eventTopic);

        if (isClientConnected()){
            try{
                client.subscribe(eventTopic, qos,  userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotUnsubscribeEvent(String event, String format, Object userContext, IMqttActionListener listener) {

        String eventTopic = getEventTopic(event,format);

        if(isClientConnected()){
            try{
                client.unsubscribe(eventTopic, userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotPublishEvent(String event, String format, Object userContext, String payload, int qos, boolean retained, IMqttActionListener listener) {

        String eventTopic = getEventTopic(event, format);
        Log.d(TAG, "Event Topic: " +eventTopic);

        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(qos);
        message.setRetained(retained);

        if(isClientConnected()){
            try {
                client.publish(eventTopic, message, userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotPublishEvent(String event, String format, String deviceType, String deviceId, Object userContext, String payload, int qos, boolean retained, IMqttActionListener listener) {

        String eventTopic = getEventTopic(event, deviceType, deviceId, format);
        Log.d(TAG, "Event Topic: " +eventTopic);

        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(qos);
        message.setRetained(retained);

        if(isClientConnected()){
            try {
                client.publish(eventTopic, message, userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }

    }

    public void watsonIotPublishCommand(String command, String format, Object userContext, String payload, int qos, boolean retained, IMqttActionListener listener) {

        String commandTopic = getCommandTopic(command, format);
        Log.d(TAG, "Published command topic: " +commandTopic);

        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(qos);
        message.setRetained(retained);

        if(isClientConnected()){
            try{
                client.publish(commandTopic, message, userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }
    }

    public void watsonIotPublishCommand(String command, String format, String deviceType, String deviceId, Object userContext, String payload, int qos, boolean retained, IMqttActionListener listener) {

        String commandTopic = getCommandTopic(command, deviceType, deviceId, format);
        Log.d(TAG, "Published command topic: " +commandTopic);

        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(qos);
        message.setRetained(retained);

        if(isClientConnected()){
            try{
                client.publish(commandTopic, message, userContext, listener);
            }
            catch(MqttException e){
                Log.d(TAG, "MQTT Exception: " + e.getMessage());
            }
        }
    }

    private static String getCommandTopic(String command, String format){
        return "iot-2/cmd/" + command + "/fmt/" + format;
    }

    private static String getCommandTopic(String command, String deviceType, String deviceId, String format){
        return "iot-2/type/" +deviceType+ "/id/" +deviceId+ "/cmd/" + command + "/fmt/" + format;
    }

    private static String getEventTopic(String event, String format){
        return "iot-2/evt/" +event+ "/fmt/" +format;
    }

    private static String getEventTopic(String event, String deviceType, String deviceId, String format){
        return "iot-2/type/" + deviceType + "/id/" + deviceId + "/evt/" + event + "/fmt/" + format;
    }

    private boolean isClientConnected(){
        boolean connected = false;
        try{
            if(client != null)
            {
                if(client.isConnected()){
                    connected = true;
                }
            }
        }
        catch (Exception e){
            Log.d(TAG, "Client is not available..");
        }
        return connected;
    }

}
