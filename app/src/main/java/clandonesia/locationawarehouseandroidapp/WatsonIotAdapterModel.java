package clandonesia.locationawarehouseandroidapp;

/**
 * Created by alfa on 2/11/17.
 */

public class WatsonIotAdapterModel {

    /* Parameters needed for connecting App to Watson IoT Platform */
    private String organization;
    private String deviceType;
    private String deviceId;
    private String username;
    private String authToken;

    /* Basic methods */
    public String getOrganization(){
        return organization;
    }
    public void setOrganization(String organization){
        this.organization = organization;
    }
    public String getDeviceType(){
        return deviceType;
    }
    public void setDeviceType(String deviceType){
        this.deviceType = deviceType;
    }
    public String getDeviceId(){
        return deviceId;
    }
    public void setDeviceId(String deviceId){
        this.deviceId = deviceId;
    }
    public String getAuthToken(){
        return authToken;
    }
    public void setAuthToken(String authToken){
        this.authToken = authToken;
    }
    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username = username;
    }
}
