package clandonesia.locationawarehouseandroidapp;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Created by Lenovo on 03/03/2017.
 */

public class LocationTrackerIntentService extends IntentService {

    private String TAG = LocationTrackerIntentService.class.getName().toString();

    private WatsonIotAdapterImpl iotAdapter;
    private MyWatsonIotAdapterListener iotAdapterListener;

    public LocationTrackerIntentService(){
        super(LocationTrackerIntentService.class.getName());
        if(iotAdapter == null){
            iotAdapter = WatsonIotAdapterImpl.getInstance(this);
        }
        Log.d(TAG, "LocationTrackerIntentService entered..");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if(geofencingEvent.hasError()){
            String errorMsg = getGeofenceErrorMessages(geofencingEvent.getErrorCode());
            Log.d(TAG, errorMsg);
            return;
        }

        int geoFenceTransition = geofencingEvent.getGeofenceTransition();

        if(geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geoFenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL){
            Log.d(TAG, "User is being inside geofence");
            //Send command for turning on power to Watson
            //iotAdapter.watsonIotPublishCommand("turn_peripheral", "json", "publish", IoTMessageProcessor.setDeviceStatusMessage(1000001, true), 0, false, getWatsonIotListener());
            iotAdapter.watsonIotPublishCommand("turn_peripheral", "json", "Home_Controller", "1000001", "publish", IoTMessageProcessor.setDeviceStatusMessage(1000001, true), 0, false, getWatsonIotListener());
        }
        else{
            Log.d(TAG, "User is being outside geofence");
            //Send command for turning off power to Watson
            iotAdapter.watsonIotPublishCommand("turn_peripheral", "json", "Android", "+", "publish", IoTMessageProcessor.setDeviceStatusMessage(1000001, false), 0, false, getWatsonIotListener());
        }
    }

    private String getGeofenceErrorMessages(int errorCode){
        String errorMsg;

        if(errorCode == Constants.GEOFENCE_NOT_AVAILABLE)
            errorMsg = "Geofence not available";
        else if(errorCode == Constants.GEOFENCE_TOO_MANY_GEOFENCES)
            errorMsg = "Too many geofences";
        else if(errorCode == Constants.GEOFENCE_TOO_MANY_PENDING_INTENTS)
            errorMsg = "Too many pending intents";
        else{
            errorMsg = "Unspecified error";
        }

        return errorMsg;
    }

    private MyWatsonIotAdapterListener getWatsonIotListener(){
        if(iotAdapterListener == null){
            iotAdapterListener = new MyWatsonIotAdapterListener(getApplicationContext(), Constants.IotAction.PUBLISH);
        }
        return iotAdapterListener;
    }
}
