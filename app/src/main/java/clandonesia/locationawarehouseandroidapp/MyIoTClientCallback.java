package clandonesia.locationawarehouseandroidapp;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by Lenovo on 16/03/2017.
 */

public class MyIoTClientCallback implements IoTClientCallback {

    private final String TAG = MyIoTClientCallback.class.getName();

    private static MyIoTClientCallback myIoTClientCallback;
    private Context context;

    private MyIoTClientCallback(Context context){
        this.context = context;
    }

    public static MyIoTClientCallback getInstance(Context context){
        if(myIoTClientCallback == null){
            myIoTClientCallback = new MyIoTClientCallback(context);
            return myIoTClientCallback;
        }
        return myIoTClientCallback;
    }

    @Override
    public void connectionLost(Throwable cause) {
        Log.d(TAG, "Connection is lost, cause:" + cause);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        Log.d(TAG, "Delivery is complete");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        String payload = new String(message.getPayload());
        Log.d(TAG, "Message arrived, topic: " + topic + ", message: " + payload);
        IoTMessageProcessor.getInstance(context).process(payload, topic);
    }
}
