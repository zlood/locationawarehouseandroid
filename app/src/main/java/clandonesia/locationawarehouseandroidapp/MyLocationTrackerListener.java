package clandonesia.locationawarehouseandroidapp;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Result;

/**
 * Created by alfa on 2/26/17.
 */

public class MyLocationTrackerListener implements LocationTrackerListener {

    private final static String TAG = MyLocationTrackerListener.class.toString();
    private final Context context;

    public MyLocationTrackerListener(Context context){
        this.context = context;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG,"On Connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"On Connection Suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG,"On ConnectionFailed, cause:" + connectionResult.toString());
    }

    @Override
    public void onResult(Result result) {
        Log.d(TAG, "On Result");
        if(result.getStatus().isSuccess()){
            Log.d(TAG, "Geofence is successfully removed");
        }
        else{
            Log.d(TAG, "Error removing geofence, cause: " + result.getStatus().getStatusMessage());
        }
    }
}
