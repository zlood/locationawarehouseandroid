package clandonesia.locationawarehouseandroidapp;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.ToggleButton;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends FragmentActivity {

    private final static String TAG = MainActivity.class.toString();

    private Location centreOfLocation;
    private LocationTracker locationTracker;
    private Context context;
    private Switch trackerSwitch;
    private ToggleButton toggleButton;

    BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isPermissionGranted()){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }

        context = this.getApplicationContext();
        final WatsonIotAdapterImpl iotadapter = WatsonIotAdapterImpl.getInstance(this.getApplicationContext());

        trackerSwitch = (Switch) findViewById(R.id.trackerSwitch);
        trackerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked){
                    iotadapter.watsonIotConnect();
                }
                else{
                    if(LocationTracker.getInstance(context) != null){
                        locationTracker = locationTracker.getInstance(context);
                        locationTracker.stopLocationTracker(context);
                        iotadapter.watsonIotDisconnect();
                    }
                }
            }
        });

        if(broadcastReceiver == null){

            Log.d(TAG, ".onCreate() - Registering iotBroadcastReceiver");
            BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(TAG, ".onReceive() - Received intent for iotBroadcastReceiver");
                    processIntent(intent);
                }
            };

            this.registerReceiver(broadcastReceiver,
                    new IntentFilter(Constants.APP_ID + Constants.INTENT_DATA_RECEIVED));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isPermissionGranted(){

        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == this.getPackageManager().PERMISSION_GRANTED);

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == this.getPackageManager().PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void processIntent(Intent intent){
        //tobe implemented
        //Implement Intent for processing: INTENT_IOT_DEVICE_STATUS, deviceName, deviceStatus
        String data = intent.getStringExtra(Constants.INTENT_DATA);
    }
}
