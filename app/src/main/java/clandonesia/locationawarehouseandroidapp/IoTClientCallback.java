package clandonesia.locationawarehouseandroidapp;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Created by Lenovo on 16/03/2017.
 */

public interface IoTClientCallback extends MqttCallback {
    @Override
    void connectionLost(Throwable cause);

    @Override
    void deliveryComplete(IMqttDeliveryToken token);

    @Override
    void messageArrived(String topic, MqttMessage message) throws Exception;
}
