package clandonesia.locationawarehouseandroidapp;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by alfa on 2/26/17.
 */

public class LocationTracker implements GoogleApiClient.ConnectionCallbacks, ResultCallback<Status> {

    private final static String TAG = LocationTracker.class.getName();

    private static LocationTracker mLocationTracker;
    private Context context;
    private PendingIntent geoFencePendingIntent;
    private MyLocationTrackerListener listener;

    private GoogleApiClient mGoogleApiClient;
    private Geofence mGeofence;
    private GeofencingRequest mGeofenceRequest;
    private Location centreOfGeofence;
    private float geofenceRadius;

    private LocationTracker (Context context, Location centreOfGeofence, float geofenceRadius){
        this.context = context;
        setCentreOfGeofence(centreOfGeofence);
        setGeofenceRadius(geofenceRadius);

        listener = new MyLocationTrackerListener(context);
        if(mGoogleApiClient == null) {

            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(listener)
                    .addApi(LocationServices.API)
                    .build();

            mGoogleApiClient.connect();
            Log.d(TAG, "Initiating google api client");

        }

        buildGeofence(Constants.GEOFENCE_REQUEST_ID, getCentreOfGeofence(),getGeofenceRadius());
    }

    public static LocationTracker getInstance(Context context){
        return mLocationTracker;
    }

    public static LocationTracker initLocationTracker(Context context, Location centreOfGeofence, float geofenceRadius){
        if(mLocationTracker == null){
            mLocationTracker = new LocationTracker(context, centreOfGeofence, geofenceRadius);
            Log.d(TAG, "LocationTracker initialized");
        }
        return mLocationTracker;
    }

    public LocationTracker stopLocationTracker(Context context){
        if(mLocationTracker != null){
            LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, getGeofencePendingIntent(context)).setResultCallback(listener);
            mLocationTracker = null;
            return mLocationTracker;
        }
        return null;
    }

    private Geofence buildGeofence(String reqId, Location centreOfGeofence,float geofenceRadius){
        if(mGeofence == null){
            mGeofence = new Geofence.Builder()
                    .setRequestId(reqId)
                    .setCircularRegion(centreOfGeofence.getLatitude(), centreOfGeofence.getLongitude(), geofenceRadius)
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .setLoiteringDelay(10000)
                    .build();
            Log.d(TAG, "Geofence Built");
        }
        return mGeofence;
    }

    public GeofencingRequest getGeoFencingRequest(){
        GeofencingRequest.Builder mGeoReqBuilder = new GeofencingRequest.Builder();
        mGeoReqBuilder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        mGeoReqBuilder.addGeofence(mGeofence);
        return mGeoReqBuilder.build();
    }

    private PendingIntent getGeofencePendingIntent(Context context){
        if(geoFencePendingIntent != null){
            Log.d(TAG, "Use instantiated pending intent");
            return geoFencePendingIntent;
        }
        Intent intent = new Intent(context, LocationTrackerIntentService.class);
        Log.d(TAG, "Pending Intent created..");
        return  PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void setCentreOfGeofence(Location centreOfGeofence){
        this.centreOfGeofence = centreOfGeofence;
    }

    private Location getCentreOfGeofence(){
        return this.centreOfGeofence;
    }

    private void setGeofenceRadius(float geofenceRadius){
        this.geofenceRadius = geofenceRadius;
    }

    private float getGeofenceRadius(){
        return this.geofenceRadius;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "On Connected from GoogleApiClient Callback");
        LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, getGeoFencingRequest(),getGeofencePendingIntent(context)).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "On Connection Suspended from GoogleApiClient Callback");
    }

    @Override
    public void onResult(Status status) {
        if(status.isSuccess()){
            Log.d(TAG, "Geofence creation is success, status: " + status.getStatusCode());
        }
        else{
            Log.d(TAG, "Geofence creation is failed, status: " + status.getStatusCode());
        }
    }
}
