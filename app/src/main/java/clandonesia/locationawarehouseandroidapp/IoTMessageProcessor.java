package clandonesia.locationawarehouseandroidapp;

/**
 * Created by Lenovo on 16/03/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class IoTMessageProcessor {

    private final static String TAG = IoTMessageProcessor.class.toString();

    private static IoTMessageProcessor instance;
    private Context context;

    private IoTMessageProcessor(Context context){
        this.context = context;
    }

    public static IoTMessageProcessor getInstance(Context context){
        if(instance == null){
            instance = new IoTMessageProcessor(context);
            return instance;
        }
        return instance;
    }

    public void process(String payload, String topic) throws JSONException{
        JSONObject top = new JSONObject(payload);
        JSONObject d = top.getJSONObject("d");

        if(topic.toLowerCase().contains(Constants.IOTDEVICE_STATUS_EVENT.toLowerCase())){
            Log.d(TAG,"From Message Processor... Topic: " +topic);
            int deviceId = d.getInt("deviceid");
            int deviceStatus = d.getInt("devicestatus");
            Log.d(TAG, "Parsed message, deviceid: " + deviceId + ", status: " +deviceStatus);

            /* Implement an intent here to update Activity/UI */
            Intent intent = new Intent(Constants.APP_ID + Constants.INTENT_DATA_RECEIVED);
            intent.putExtra(Constants.INTENT_DATA, Constants.INTENT_IOT_DEVICE_STATUS);
            intent.putExtra(Constants.INTENT_IOT_DEVICE_STATUS, deviceStatus);
            intent.putExtra(Constants.INTENT_DEVICE_ID, deviceId);
            context.sendBroadcast(intent);
            Log.d(TAG,"Intent Broadcast sent..");
        }
    }

    public static String setDeviceStatusMessage(int deviceId, boolean deviceStatus) {
        int status;
        if(deviceStatus){
            status = 1;
        }
        else{
            status = 0;
        }
        return "{\"d\":{" +
                "\"deviceid\":" + deviceId + "," +
                "\"devicestatus\":" + status +
                "}}";
    }

    public static String setTextMessage(String text) {
        return "{\"d\":{" +
                "\"text\":\"" + text + "\"" +
                " } }";
    }

}
