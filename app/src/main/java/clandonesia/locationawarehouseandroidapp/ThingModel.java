package clandonesia.locationawarehouseandroidapp;

/**
 * Created by alfa on 2/12/17.
 */

public class ThingModel {

    private String thingName;
    private String thingId;
    private String peripheralId;
    private Boolean thingStatus;

    public String getThingName(){
        return thingName;
    }
    public void setThingName(String thingName){
        this.thingName = thingName;
    }
    public String getThingId(){
        return thingId;
    }
    public void setThingId(String thingId){
        this.thingId = thingId;
    }
    public String getPeripheralId(){
        return peripheralId;
    }
    public void setPeripheralId(String peripheralId){
        this.peripheralId = peripheralId;
    }
    public Boolean getThingStatus(){
        return thingStatus;
    }
    public void setThingStatus(Boolean thingStatus){
        this.thingStatus = thingStatus;
    }

}
